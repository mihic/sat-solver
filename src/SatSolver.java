import java.io.FileReader;
import java.io.IOException;
import java.util.*;


/**
 * Created by Duzl Studios on 2016-03-16.
 */


public class SatSolver {

    //sosednostni seznam: clause vsebuje linke do vseh var, ki jih vsebuje, var pa ima sezanm vseh caluse v katerih nastopa.
    static class Clause {
        Integer ID;
        TreeSet<Integer> lits;

        Clause(int n) {
            lits = new TreeSet<>();
            ID = n;
        }

        public String toString() {
            StringJoiner joiner = new StringJoiner(" ˅ ", "(", ")");
            joiner.setEmptyValue("<empty>");
            for (Integer l : lits) {
                joiner.add(l.toString());
            }
            return joiner.toString();
        }
    }

    static class Var {
        Map<Integer, Clause> appearsIn;

        Var() {
            appearsIn = new HashMap<>();
        }
    }

    static int nbvar;
    static int nbclauses;
    static int n = 0;
    static ArrayList<Var> vars;
    static Map<Integer, Clause> clauses = new HashMap<>();
    static LinkedList<Clause> units = new LinkedList<>();
    static TreeSet<Integer> assignments = new TreeSet<>();

    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("Missing argument: file");
        } else {
            try {
                // branje vhodne datoteke
                Scanner sc = new Scanner(new FileReader(args[0]));
                while (sc.hasNext()) {
                    if (sc.hasNext("c")) sc.nextLine();
                    else if (sc.hasNext("p")) {
                        sc.next("p");
                        sc.next("cnf");
                        nbvar = sc.nextInt();
                        nbclauses = sc.nextInt();
                        //System.out.printf("%d,%d\n", nbvar, nbclauses);
                        vars = new ArrayList<>(nbvar + 1);
                        for (int i = 0; i <= nbvar; i++) {
                            vars.add(new Var());
                        }
                    } else if (sc.hasNextInt()) {
                        Clause c = new Clause(n++);
                        while (true) {
                            int v = sc.nextInt();
                            if (v == 0) break;
                            c.lits.add(v);
                            vars.get(Math.abs(v)).appearsIn.put(c.ID, c);
                        }
                        clauses.put(c.ID, c);
                    }
                }
                // inicializacija seznama unitov
                for (Clause c : clauses.values()) {
                    if (c.lits.size() == 1) {
                        units.add(c);
                    }
                }
                if (dpll()) {
                    for (Integer i : assignments) System.out.printf("%d ", i);
                } else {
                    System.out.print("No solution found.");
                }
            } catch (IOException e) {
                System.out.println(e);
            }
        }
    }

    static void printFormula() {
        StringJoiner joiner = new StringJoiner(" ˄ ", "[", "]");
        joiner.setEmptyValue("<empty>");
        for (Clause c : clauses.values()) {
            joiner.add(c.toString());
        }
        System.out.println(joiner.toString());
    }

    // poenostavitev izraza za določen literal, spremebe zapiši v seznam sprememb
    static boolean simplify(int literal, LinkedList<Change> changes) {
        Iterator<Clause> it = vars.get(Math.abs(literal)).appearsIn.values().iterator();
        while (it.hasNext()) {
            Clause c = it.next();
            if (c.lits.contains(literal)) { // odstrani celoten clause
                for (Integer l : c.lits) {
                    if (Math.abs(l) != Math.abs(literal)) { //da ne pokvarimo iteratorja
                        vars.get(Math.abs(l)).appearsIn.remove(c.ID);
                    }
                }
                clauses.remove(c.ID);
                changes.addFirst(new RemovedClause(c));
                if (c.lits.size() == 1) units.remove(c);
            } else if (c.lits.contains(-1 * literal)) { // odstrani le literal
                c.lits.remove(new Integer(-1 * literal));
                changes.addFirst(new RemovedLiteral(c.ID, -1 * literal));
                switch (c.lits.size()) {
                    case 0:
                        units.remove(c);
                        return false;
                    case 1:
                        units.add(c);
                }
            }
        }
        vars.get(Math.abs(literal)).appearsIn.clear();
        return true;
    }

    // dpll algoritem
    static boolean dpll() {
        LinkedList<Change> changes = new LinkedList<>();
        // najprej poenostavi unite
        while (!units.isEmpty()) {
            Clause unit = units.getFirst();
            int lit = unit.lits.first();
            if (!simplify(lit, changes)) {
                rollback_changes(changes);
                return false;
            }
            assignments.add(lit);
            changes.add(new AddedAssigment(lit));
        }
        if (clauses.size() == 0) return true; //zmaga!!

        // ko zmanjka unitov, izberi en var, ga poenostavi in rekurzivno kliči dpll
        LinkedList<Change> guessed_changes = new LinkedList<>();
        Integer lit = clauses.values().iterator().next().lits.first();
        if (simplify(lit, guessed_changes) && dpll()) {
            assignments.add(lit);
            return true;
        } else {
            // izbrani var ni vredu, zato razveljavi spremembe, poskusi z negiranim
            rollback_changes(guessed_changes);
            lit = lit * -1;
            guessed_changes = new LinkedList<>();
            if (simplify(lit, guessed_changes) && dpll()) {
                assignments.add(lit);
                return true;
            } else {
                // negiran izbrani var ni vredu, trenutna formula ni zadovoljiva, počistimo za sabo in vrnemo false
                rollback_changes(guessed_changes);
                rollback_changes(changes);
                return false;
            }
        }

    }

    static void rollback_changes(LinkedList<Change> cs) {
        for (Change c : cs) c.rollback();
    }

    static abstract class Change {
        public abstract void rollback();
    }

    static class RemovedClause extends Change {
        Clause clause;

        public RemovedClause(Clause clause) {
            this.clause = clause;
        }

        @Override
        public void rollback() {
            clauses.put(clause.ID, clause);
            if (clause.lits.size() == 1) {
                units.add(clause);
            }
            for (int lit : clause.lits) {
                vars.get(Math.abs(lit)).appearsIn.put(clause.ID, clause);
            }
        }
    }

    static class RemovedLiteral extends Change {
        int clauseID;
        int literal;

        public RemovedLiteral(int clauseID, int literal) {
            this.clauseID = clauseID;
            this.literal = literal;
        }

        @Override
        public void rollback() {
            Clause c = clauses.get(clauseID);
            if (c.lits.size() == 1) {
                units.remove(c);
            }
            vars.get(Math.abs(literal)).appearsIn.put(clauseID, c);
            c.lits.add(literal);
            if (c.lits.size() == 1) {
                units.add(c);
            }
        }
    }

    static class AddedAssigment extends Change {
        int lit;

        public AddedAssigment(int lit) {
            this.lit = lit;
        }

        @Override
        public void rollback() {
            assignments.remove(lit);
        }
    }

}