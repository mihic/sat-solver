
import qualified Data.Map.Strict as Map

data Formula = And [Formula]
             | Or [Formula]
             | Not Formula
             | Imp Formula Formula
             | Var String
             | Tru
             | Fls
    deriving (Eq, Show)

eval :: Formula -> Map.Map [Char] Bool -> Bool
eval f lst = case f of 
        Not a        -> not $ eval a lst
        Tru          -> True
        Fls          -> False
        Var str      -> lst Map.! str
        Imp a b      -> eval (Or [Not a,b]) lst
        And (hd:tl)  -> let b = eval hd lst
                        in case b of
                            True -> eval (And tl) lst
                            False -> False
        And []       -> True
        Or (hd:tl)   -> let b = eval hd lst
                        in case b of 
                            True -> True
                            False -> eval (Or tl) lst
        Or []        -> False    

sez = Map.empty

mapNotAndSimplify l = map (\x-> Not $ simplify x) l
simplify f = case f of
    Not (Or l) -> simplify $ And $ mapNotAndSimplify l
    Not (And l) -> simplify $ Or $ mapNotAndSimplify l
    Not (Not a) -> simplify a
    Not Tru     -> Fls
    Not Fls     -> Tru
    Imp a b     -> simplify $ Or [Not a,b]
    And l       -> let ls = map simplify l
                        in if elem Fls ls 
                            then Fls
                            else let flt = filter (\x->x/=Tru) ls
                                in case flt of 
                                    x:[] -> x
                                    []   -> Tru
                                    _    -> And flt
    Or l       -> let ls = map simplify l
                        in if elem Tru ls 
                            then Tru
                            else let flt = filter (\x->x/=Fls) ls
                                in case flt of 
                                    x:[] -> x
                                    []   -> Fls
                                    _    -> Or flt                        
    x           -> x
